﻿using Snoop.ExcelHandler.Core.Creator;
using System;
using System.Collections.Generic;

namespace Snoop.ExcelHandler.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelCreator excel = new ExcelCreator();
            var content = new List<List<Cell>>();
            content.Add(Helpers.GetCells("hola", "chau"));
            excel.AddSheet(new Sheet
            {
                Content = content,
                Name = "hoja1",
                ShowHeaders = false
            });
            excel.FileName = @"C:\Users\Facundo\Desktop\archivo.xlsx";
            if (!excel.CreateExcelFile())
            {
                Console.WriteLine(excel.ExceptionMessage);
            }

            Console.ReadKey();
        }
    }
}
