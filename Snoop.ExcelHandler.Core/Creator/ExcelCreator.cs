﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;

namespace Snoop.ExcelHandler.Core.Creator
{
    public class ExcelCreator
    {
        public ExcelCreator()
        {
            _config = Common.Helpers.ReadJsonFile<Config>(Config.PathJsonConfig);
            _messages = Common.Helpers.ReadJsonFile<Messages>(Path.Combine(_config.PathLanguage, _config.Language + ".json"));
        }

        #region Fields
        string _fileName = "Book.xlsx";
        readonly List<Sheet> _sheets = new List<Sheet>();
        readonly Config _config;
        readonly Messages _messages;
        #endregion

        #region Properties
        public string FileName
        {
            get => _fileName.EndsWith(".xlsx") ? _fileName : _fileName + ".xlsx";
            set => _fileName = value;
        }
        public string Password { get; set; }
        public string Error { get; private set; }
        public string ExceptionMessage { get; private set; }
        #endregion


        public bool AddSheet(Sheet sheet)
        {
            var result = true;
            if (sheet == null) throw new ArgumentNullException(nameof(sheet));
            if (sheet.Content == null)
            {
                ExceptionMessage = _messages.NoContent;
                result = false;
            } 
            if (string.IsNullOrEmpty(sheet.Name)) sheet.Name = _config.DefaultSheetName;
            _sheets.Add(sheet);
            return result;
        }

        /// <summary>
        /// Clean error messages
        /// </summary>
        void Reset()
        {
            Error = string.Empty;
            ExceptionMessage = string.Empty;
        }

        /// <summary>
        /// Generates byte array from Excel generated
        /// </summary>
        /// <returns>array de bytes del excel</returns>
        public byte[] GenerarExcel()
        {
            Reset();
            var excel = _createExcel();
            var ar = excel.GetAsByteArray();
            if (ar == null)
            {
                Error = _messages.FailGenerateExcel;
            }
            return ar;
        }

        /// <summary>
        /// Creates Excel file and writes it on FileName path
        /// </summary>
        /// <returns>operation result</returns>
        public bool CreateExcelFile()
        {
            try
            {
                Reset();
                if (string.IsNullOrEmpty(FileName))
                {
                    Error = _messages.NoFileName;
                    return false;
                }
                var excel = _createExcel();
                var file = new FileInfo(FileName);
                if (string.IsNullOrEmpty(Password))
                {
                    excel.SaveAs(file);
                }
                else
                {
                    excel.SaveAs(file, Password);
                }

                return true;
            }
            catch (Exception e)
            {
                Error = e.Message;
                return false;
            }
        }

        ExcelPackage _createExcel()
        {
            ExcelPackage excel = new ExcelPackage();
            foreach (var hoja in _sheets)
            {
                int fila = 1, columna = 1;
                var sheet = excel.Workbook.Worksheets.Add(hoja.Name);

                foreach (var titulo in hoja.Headers)
                {
                    sheet.Cells[fila, columna].WritesCell(titulo);
                    columna++;
                }
                if (hoja.Headers.Count > 0)
                {
                    fila++;
                    columna = 1;
                }

                foreach (var registro in hoja.Content)
                {
                    foreach (var celda in registro)
                    {
                        sheet.Cells[fila, columna].WritesCell(celda);
                        columna++;
                    }
                    fila++; columna = 1;
                }
            }
            return excel;
        }
    }
}