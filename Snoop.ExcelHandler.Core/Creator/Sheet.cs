﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Snoop.ExcelHandler.Core.Creator
{
    public class Sheet
    {
        public bool ShowHeaders { get; set; }
        public List<Cell> Headers { get; set; }
        public string Name { get; set; }
        public List<List<Cell>> Content { get; set; }

        
        public static Sheet CreateSheet<T>(List<T> datasource, Func<T, List<Cell>> parseMethod = null)
        {
            var sheet = new Sheet
            {
                Content = parseMethod == null ?
                Helpers.ParseMethodDefault(datasource) :
                datasource.Select(d => parseMethod(d)).ToList()
            };

            return sheet;
        }
    }
}