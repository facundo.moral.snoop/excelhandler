﻿using System;

namespace Snoop.ExcelHandler.Core.Creator
{
    public class Cell
    {
        public string Value { get; set; }
        public bool Bold { get; set; } = false;
        public bool Underline { get; set; } = false;
        public bool Strike { get; set; } = false;
        public bool Border { get; set; } = false;
    }
}
