﻿using System;

namespace Snoop.ExcelHandler.Core.Creator
{
    public class Messages
    {
        public string NoContent { get; set; }
        public string NoContentNoDatasource { get; set; }
        public string NoParser { get; set; }
        public string NoConfig { get; set; }
        public string FailAddSheet { get; set; }
        public string FailGenerateExcel { get; set; }
        public string NoFileName { get; set; }
    }
}
