﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Snoop.ExcelHandler.Core.Creator
{
    public static class Helpers
    {
        public static void WritesCell(this ExcelRange range, Cell cell)
        {
            range.Value = cell.Value;
            range.Style.Font.Bold = cell.Bold;
            range.Style.Font.UnderLine = cell.Underline;
            range.Style.Font.Strike = cell.Strike;
            if (cell.Border)
            {
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            }
        }

        public static List<Cell> GetCells(params object[] valores)
            => valores.Select(v => new Cell { Value = v.ToString() }).ToList();

        public static List<Cell> GetTitulos<T>()
        {
            return typeof(T).GetProperties().Select(p => new Cell { Value = p.Name }).ToList();   
        }

        public static List<List<Cell>> ParseMethodDefault<T>(List<T> datasource)
        {
            var hoja = new List<List<Cell>>();
            var t = datasource.FirstOrDefault();
            Type tipo = t.GetType();
            List<PropertyInfo> props = new List<PropertyInfo>(tipo.GetProperties());

            foreach (var data in datasource)
            {
                var fila = new List<Cell>();
                foreach (PropertyInfo prop in props)
                {
                    fila.Add(new Cell
                    {
                        Value = prop.GetValue(data, null).ToString()
                    });
                }
                hoja.Add(fila);
            }
            return hoja;
        }

    }
}
