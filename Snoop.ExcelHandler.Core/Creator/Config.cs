﻿using System;
using System.IO;

namespace Snoop.ExcelHandler.Core.Creator
{
    public class Config
    {
        public string PathLanguage { get; set; }
        public string DefaultFileName { get; set; }
        public string Language { get; set; }
        public string DefaultSheetName { get; set; }

        public static string PathJsonConfig = Path.Combine(Common.Helpers.GetExecutingPath() + "Creator/JSON/ExcelConfig.json");
    }
}